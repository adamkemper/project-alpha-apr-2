from django.contrib import admin
from tasks.models import Task


class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
    list_filter = ("is_completed", "project", "assignee")
    search_fields = ("name",)
    date_hierarchy = "due_date"
    raw_id_fields = ("project", "assignee")


admin.site.register(Task, TaskAdmin)
