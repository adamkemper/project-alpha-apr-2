from django.urls import path
from projects.views import list_projects
from projects.views import show_project
from projects import views


urlpatterns = [
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("", list_projects, name="list_projects"),
]
