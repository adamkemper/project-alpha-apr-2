from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from accounts.forms import ProjectForm


def show_project(request, id):
    project = get_object_or_404(Project, pk=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def list_projects(request):
    members = Project.objects.filter(owner=request.user)
    user = {"projects": members}
    return render(request, "projects/project_list.html", user)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")  # Redirect to project list
    else:
        form = ProjectForm()
    return render(request, "create_project.html", {"form": form})
