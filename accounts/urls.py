from django.urls import path
from accounts.views import login_user
from accounts.views import user_logout
from accounts.views import signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", login_user, name="login"),
    path("logout/", user_logout, name="logout"),
]
